package com.example

import com.google.gson.Gson
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.features.*
import org.slf4j.event.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import java.net.URL
import kotlinx.coroutines.*
import io.ktor.client.features.logging.*
import java.io.File
import java.nio.file.Paths
import com.google.gson.reflect.TypeToken



fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)
private val server = "http://192.168.56.101:8080/"
private val SPLASH_STR = "/"
//private val server = "http://0.0.0.0:8080/"

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    val path = System.getProperty("user.dir")

    println("Working Directory = $path")

    val client = HttpClient(Apache) {
        install(JsonFeature) {
            serializer = GsonSerializer()
        }
        install(Logging) {
            level = LogLevel.HEADERS
        }
        expectSuccess = true
    }

    runBlocking {
        val result = client.post<Unit> {
            url(URL(server+"cluster/"))
            contentType(ContentType.Application.Json)
        }

        println ("register cluster: "+result)
    }

    routing {
        route ("file/{filename}/") {
            get {
                val fileName = call.parameters.get("filename")
                println ("get file "+fileName!!)

                val pos = call.request.queryParameters.getAll("pos")
                val data = HashMap<Int, String>()
                pos?.forEach {
                    println (fileName+" "+pos+" "+getFile (fileName!!, it))
                    data.put (
                        it.toInt(),
                        getFile (fileName!!, it))
                }
                call.respond (HttpStatusCode.OK, Gson().toJson(data))
            }

            post {
                val fileName = call.parameters.get("filename")
                println ("post file "+fileName!!)

                if (!File(fileName).isDirectory)
                    createDir (fileName!!)

                val mapType = object : TypeToken<Map<Int, String>>(){}.type

                val data = Gson().fromJson<Map<Int,String>> (
                    call.receiveParameters().get("data"),
                    mapType)

                data.entries.forEach {
                    println (fileName+" "+it.key+" "+it.value)
                    writeFile (fileName!!, it.key, it.value)
                }

                call.respond (HttpStatusCode.OK)
            }
            delete {
                val fileName = call.parameters.get("filename")
                println ("delete file "+fileName!!)
                File (fileName).deleteRecursively()

                call.respond (HttpStatusCode.OK)
            }

        }
    }
}

private fun createDir (name: String): Boolean {
    val res = File (Paths.get(name).toUri()).mkdir()
    println("create dir "+name+" "+res)
    return res
}

private fun getFile (dir: String, pos: String): String =
    File (dir+ SPLASH_STR+pos).readText()

private fun writeFile (dir: String,
                       pos: Int,
                       data:String) =
    File (dir+ SPLASH_STR+pos).writeText (data)

data class JsonSampleClass(val hello: String)

