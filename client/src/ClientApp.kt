package com.example

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import java.net.URL
import kotlinx.coroutines.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.forms.FormDataContent
import java.lang.IllegalArgumentException

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)
private lateinit var client: HttpClient
private val server = "http://192.168.56.101:8080/"

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    client = HttpClient(Apache) {
        install(JsonFeature) {
            serializer = GsonSerializer()
        }
        install(Logging) {
            level = LogLevel.HEADERS
        }
        expectSuccess = true
    }



    println ("input command")
    for (line in System.`in`.bufferedReader().lines()) {
        val parts = line.split(" ")
        println (parts.size)
        println (Command.GET)
        try {
            when (Command.valueOf (parts.first())) {
                Command.HELP -> help()
                Command.GET -> when {
                    parts.size == 2 -> get(parts[1], 0, -1)
                    parts.size == 4 -> get(parts[1], parts[2].toInt(), parts[3].toInt())
                    else -> println ("not valid params")
                }
                Command.DELETE -> when {
                    (parts.size == 2) -> delete (parts[1])
                    else -> println ("not valid params")
                }
                Command.GET_ALL -> when {
                    (parts.size == 1) -> getAll ()
                    else -> println ("not valid params")
                }
                Command.SAVE -> when {
                    (parts.size == 3) -> save (parts[1], parts[2], null)
                    (parts.size == 4) ->  save (parts[1], parts[3], pos = parts[2].toInt())
                    else -> println ("not valid params")
                }
                else -> println ("this is command")
            }
        } catch (e: IllegalArgumentException) {
            println ("not valid command")
        }
        println (line+" repeat")
    }

}

private fun help () {
    for (command in Command.values()) {
        println (command.name)
    }
}

private fun Application.get (fileName: String,
                             pos: Int,
                             count: Int) {
    runBlocking {
        val text = client.get<String> {
            url(URL(server+"file/"))
            contentType(ContentType.Application.Json)
            parameter ("file", fileName)
            parameter ("pos", pos)
            parameter ("count", count)
        }
        println ("result: "+text)
    }
}

private fun Application.delete (fileName: String) {
    runBlocking {
        val result = client.delete<Unit> {
            url(URL(server+"file/"))
            contentType(ContentType.Application.Json)
            parameter ("file", fileName)
        }
        println ("result: "+result)
    }
}

private fun Application.getAll () {
    runBlocking {
        val response = client.get<String> {
            url(URL(server+"file/"))
            contentType(ContentType.Application.Json)
        }

        val listType = object : TypeToken<List<String>>(){}.type

        val files = Gson().fromJson<List<String>> (
            response,
            listType)

        println ("count: "+files.size)
        for (fileName in files) {
            println (fileName)
        }
    }
}

private fun Application.save (fileName: String,
                             text: String,
                              pos: Int?) {
    println ("askldfkljasldkfjals")
    println ("11111111")
    println (URL(server+"file/"+fileName))
    println (fileName!!+" "+text+" "+pos.toString())
    runBlocking {
        val result = client.post<Unit> {
            url(URL(server + "file/"+fileName+"/"))
            body = FormDataContent(Parameters.build {
                if (pos != null)
                    append("pos", pos.toString())
                append("text", text)
            })
        }
        println ("result: "+text)
    }
}

data class JsonSampleClass(val hello: String)

enum class Command () {
    GET,
    SAVE,
    GET_ALL,
    DELETE,
    HELP;
}
