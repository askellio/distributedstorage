package com.example

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.features.*
import org.slf4j.event.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.client.features.json.*
import io.ktor.client.request.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.forms.FormDataContent
import io.ktor.client.utils.EmptyContent
import kotlinx.atomicfu.atomic
import java.lang.StringBuilder
import java.util.concurrent.ConcurrentHashMap

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    val client = HttpClient(Apache) {
        install(JsonFeature) {
            serializer = GsonSerializer()
        }
        install(Logging) {
            level = LogLevel.HEADERS
        }
        expectSuccess = true
    }

    val storage = ConcurrentHashMap<String, ConcurrentHashMap<Int, Int>>()
    val clusters = ConcurrentHashMap<Int, Pair<String, Int>>()
    val generator = atomic(1)
    val blockSize = 2

    routing {
        route ("file/{filename?}/") {

            delete {
                val fileName = call.parameters.get("file")
                println ("delete file "+fileName)
                deleteFile (storage, fileName, clusters, client)
                call.respond (HttpStatusCode.OK)
            }

            get {
                val str = String()

                val fileName = call.parameters.get("file")
                println ("get file "+fileName)

                if (fileName == null) {
                    call.respond (HttpStatusCode.OK, Gson().toJson(storage.keys().toList()))
                } else {
                    val params = call.parameters
                    val pos = params.get("pos")
                    val count = params.get("count")
                    println (fileName)
                    val startIndex: Int = if (params.contains("pos"))
                        params.get("pos")!!.toInt()
                    else
                        0

                    var endIndex: Int= 0
                    if (params.get("count")!!.toInt() > 0) {
                        println ("count " +params.get("count")!!.toInt())
                        endIndex = startIndex + params.get("count")!!.toInt()
                    } else {
                        var max = 0
                        for (key in storage.getValue(fileName).keys()) {
                            max = Math.max (max, key)
                            println ("max "+max)
                        }
                        endIndex = max+1
                    }

                    println ("start index = " + startIndex)
                    println ("end index = " + endIndex)

                    val clusterPos = HashMap<Int, MutableList<String>>()
                    val file = storage.getValue(fileName)
                    for (i in startIndex..endIndex-1) {
                        println (i)
                        if (file.containsKey(i)) {
                            val index = file.getValue(i)
                            println ("cluster = "+index+" pos="+i)
                            if (clusterPos.containsKey(index))
                                clusterPos.get(index)!!.add (i.toString())
                            else
                                clusterPos.put(index!!, mutableListOf (i.toString()))
                        }
                    }

                    val data = ConcurrentHashMap<Int, String>()

                    println ("before request get")
                    clusterPos.forEach({

                        val cluster = clusters.get(it.key)!!
                        println (cluster.first)
                        val response = client.get<String> (
                            host = cluster.first,
                            port = cluster.second,
                            path = "/file/"+fileName,
                            block = {
                                for (item in it.value)
                                    parameter ("pos", item)
                            }
//                            body = FormDataContent (parametersOf("pos", it.value))
                            )

                        val mapType = object : TypeToken<Map<Int, String>>(){}.type

                        data.putAll(
                            Gson().fromJson<Map<Int,String>> (
                                response,
                                mapType))
                    })

                    println ("after request get")

                    var max = 0
                    for (key in data.keys()) {
                        max = Math.max (max, key)
                    }

                    val str = StringBuilder ()

                    for (i in 0..max) {
                        if (data.containsKey(i))
                            str.append (data.get(i))
                    }

                    println ("result str = "+str)

                    call.respond (HttpStatusCode.OK, str.toString())
                }

            }

            post {
                val fileName = call.parameters.get("filename")
                println ("create url "+fileName)
                val params = call.receiveParameters()
                if (!params.contains("pos")) {

                    println ("before delete")
                    deleteFile (storage, fileName, clusters, client)
                    println ("after delete "+fileName)

                    val text = params.get("text")
                    var blockCount =  (text!!.length / blockSize)
                    if (text.length.rem(blockSize) > 0)
                        blockCount++
                    var countOnCluster =  blockCount / clusters.size
                    if (blockCount.rem(clusters.size) > 0)
                        countOnCluster++
                    val clusterIndexes = clusters.keys().toList()
                    val data = ConcurrentHashMap<Int, Int>()
                    for (i in 0..(clusters.size-1)) {
                        val clusterIndex = clusterIndexes.get(i)
                        val clusterData = HashMap<Int, String>()
                        for (j in 0..(countOnCluster-1)) {
                            var index = clusters.size * j + i
                            println(index.toString()+"  i="+i+"  j="+j)
                            if (index < blockCount) {
                                data.put (index, clusterIndex)
                                clusterData.put (
                                    index,
                                    text.substring (
                                        blockSize*index,
                                        Math.min (blockSize*(index+1), text.length)))
                            }
                        }

                        var cluster = clusters.get(clusterIndex)
                        println ("before save on cluster")
                        if (clusterData.isNotEmpty())
                            client.post<Unit> (
                                host = cluster!!.first,
                                port = cluster.second,
                                path = "/file/"+fileName,
                                body = FormDataContent (
                                    Parameters.build {
                                    append ("data", Gson().toJson(clusterData))
                                    }))
                    }

                    storage.put (fileName!!, data)
                } else {

                    println ("update pos text")
                    val pos = params.get("pos")!!.toInt()
                    var text = params.get("text")
                    if (text!!.length > blockSize)
                        text = text.substring (0, blockSize)
                    if (storage.containsKey(fileName!!)) {
                        val data = HashMap<Int, String> ()
                        data.put (pos, text!!)
                        val cluster = clusters.getValue (
                            storage.getValue (key=fileName!!).getValue(pos))

                        client.post<Unit> (
                            host = cluster!!.first,
                            port = cluster.second,
                            path = "/file/"+fileName,
                            body = FormDataContent (
                                Parameters.build {
                                    append ("data", Gson().toJson(data))
                                }))
                    } else
                        println ("no such file")
                }
                call.respond (HttpStatusCode.OK)
            }


        }

        get("/search/") {
            val fileName = call.parameters.get("file")
            println ("search url "+fileName)
            call.respond (HttpStatusCode.OK, storage.contains(fileName))
        }

        post("/cluster/"){
            val host = call.request.origin.remoteHost
            val port = call.request.port()
            val pos = generator.incrementAndGet()
            clusters.put(
                pos,
                Pair(host, port))
            call.respond(HttpStatusCode.OK)
            println ("register cluster "+host+" "+pos)
        }
    }
}

private suspend fun deleteFile(
    storage: ConcurrentHashMap<String, ConcurrentHashMap<Int, Int>>,
    fileName: String?,
    clusters: ConcurrentHashMap<Int, Pair<String, Int>>,
    client: HttpClient
) {
    val result = storage.containsKey(fileName)
    if (result) {
        println("in delete block")
        println("storarge contains file " + fileName)
        val hosts = HashSet<Pair<String, Int>>()
        for (index in storage.get(fileName)!!.values) {
            hosts.add(clusters.get(index)!!)
            if (hosts.size == clusters.size)
                break
        }
        hosts.forEach({
            println(it.first + " " + it.second)
            client.delete(
                host = it.first,
                port = it.second,
                path = "/file/" + fileName
            )
        })

        storage.remove(fileName)
    }
}

data class JsonSampleClass(val hello: String)

